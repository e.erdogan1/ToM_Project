class DoctorHuman:

  def __init__(self, name):
    self.name = name
    self.beliefbase = []
    self.abs_1 = "L_Y_good_communication_X"
    self.abs_2 = "L_Y_good_capabilities_X"
    self.expected_action_1 = "discuss"
    self.expected_action_2 = "agree"
    self.expected_action_3 = "persuade"
    self.expected_action_4 = "consult"
    self.relevant_beliefs = [self.abs_1, self.abs_2]
    self.trust_y_x = 0
    self.unexpected_agent_actions = 0
    self.unexpected_agent_action_alert = 0

  def init_human(self, abs_1_value, abs_2_value):
    self.beliefbase.append([self.abs_1, abs_1_value])
    self.beliefbase.append([self.abs_2, abs_2_value])
    self.abstraction()

  def add_belief(self, belief, round):
    if not self.beliefbase.__contains__(belief): #check if belief is in the beliefbase
      #print("not found")
      opposite_value = (belief[1]+1) % 2
      if self.beliefbase.__contains__([belief[0], opposite_value]):
        #if the opposite belief is in the beliefbase, revise it with the new value
        belief_index = self.beliefbase.index([belief[0], opposite_value])
        self.beliefbase[belief_index][1] = belief[1]
      else: #if the belief does not exist at all, add it to the beliefbase
        self.beliefbase.append(belief)
      
  def abstraction(self):
    if self.beliefbase.__contains__([self.abs_1, 1]) and self.beliefbase.__contains__([self.abs_2, 1]):
      self.trust_y_x = 1
    else:
      self.trust_y_x = 0

  def deliberation_critic(self, agent_action): #designed for second experiment in which agent does not trust human
    self.unexpected_agent_action_alert = 0
    self.abstraction()
    if (self.trust_y_x == 1 and agent_action == self.expected_action_4) or (self.trust_y_x == 0 and agent_action == self.expected_action_3):
      self.unexpected_agent_actions += 1
      self.unexpected_agent_action_alert = 1
      #print("alert")

  def deliberation_critic2(self, agent_action): #designed for third experiment in which agent trusts human
    self.unexpected_agent_action_alert = 0
    self.abstraction()
    if (self.trust_y_x == 1 and agent_action == self.expected_action_2) or (self.trust_y_x == 0 and agent_action == self.expected_action_1):
      self.unexpected_agent_actions += 1
      self.unexpected_agent_action_alert = 1
      #print("alert")
  
  def tell_belief_values(self, beliefs):
    answered_beliefs = []
    for i in range(0, len(beliefs)):
      if self.beliefbase.__contains__([beliefs[i], 0]):
        belief_index = self.beliefbase.index([beliefs[i], 0])
        answered_beliefs.append(self.beliefbase[belief_index])
      elif self.beliefbase.__contains__([beliefs[i], 1]):
        belief_index = self.beliefbase.index([beliefs[i], 1])
        answered_beliefs.append(self.beliefbase[belief_index])
      else:
        print ("belief is not found in the beliefbase")
    return answered_beliefs