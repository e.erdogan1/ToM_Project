# Computational Theory of Mind with Abstractions for Effective Human-Agent Collaboration - Agent Implementation and Simulations
------------------------------------------------------------------------------------------------------------------------------------
The implementation of the agents that we have described in our AAMAS'24 paper submission with id of 238 is given in this repository.
The repository also features the agent simulation experiments that have been described in the same paper.
------------------------------------------------------------------------------------------------------------------------------------
## DEPENDENCIES
- The implementation is based on Python3.
------------------------------------------------------------------------------------------------------------------------------------
## INSTRUCTIONS

- The experimentation consists of four files: Two for experiments and two for agent implementation.
	* experiment_1_2.py, experiment_3.py, doctoragent.py, doctorhuman.py

- Raw results for each experiment can be seen in the excel file "Agent_Simulation_Experiment_Results.xlsx".

- You can reproduce the results by running the experiment#.py files with the randomization seeds given in the same files.

- If you have any questions about the agent implementation or the experiments, you can contact us via anonymouscodebase@gmail.com.
